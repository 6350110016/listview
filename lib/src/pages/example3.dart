import 'package:flutter/material.dart';

class Example3 extends StatelessWidget {
  Example3({Key? key}) : super(key: key);

  final titles = ['President', 'BTS', 'Kim Namjoon', 'Kim Seokjin',
    'Min Yoongi', 'Jeong Hoseok', 'Park jimin', 'Kim taehyung', 'Jeon jungkook','Army'];

  final leading = ['asset/image/1.jpg','asset/image/2.jpg','asset/image/3.jpg','asset/image/4.jpg','asset/image/5.jpg'
    ,'asset/image/6.jpg','asset/image/7.jpg','asset/image/8.jpg','asset/image/9.jpg','asset/image/10.jpg'];

  final subtitle = ['bighit', 'BTS', 'rm', 'jin',
    'suga', 'jhope', 'jimin', 'taehyung', 'jungkook','Army'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView3'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
          itemBuilder: (context, index){
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(backgroundImage: AssetImage("${leading[index]}",), radius: 40,),
                  title: Text('${titles[index]}', style: TextStyle(fontSize: 18),),
                  subtitle: Text('${subtitle[index]}',style: TextStyle(fontSize: 15),),
                  trailing: Icon(Icons.notifications_none,size: 25,),
                ),
                Divider(thickness: 1,),
              ],
            );
          },

          ),
    );
  }
}
