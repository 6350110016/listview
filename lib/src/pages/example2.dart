import 'package:flutter/material.dart';

class Example2 extends StatelessWidget {
  const Example2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView2'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(
              Icons.directions_railway,
            size: 25,
            ),
            title: Text(
              '8.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
                Icons.notifications_none,
            size: 25,),
            onTap: (){
              print('Train');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_bike,
              size: 25,
            ),
            title: Text(
              '9.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('Bike');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_boat,
              size: 25,
            ),
            title: Text(
              '9.30 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('Boat');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_bus,
              size: 25,
            ),
            title: Text(
              '8.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('Bus');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_run,
              size: 25,
            ),
            title: Text(
              '8.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('Run');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_walk,
              size: 25,
            ),
            title: Text(
              '7.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('Walk');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_subway,
              size: 25,
            ),
            title: Text(
              '8.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('Subway');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_transit,
              size: 25,
            ),
            title: Text(
              '8.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('Transit');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.account_tree,
              size: 25,
            ),
            title: Text(
              '10.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('Tree');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.dangerous,
              size: 25,
            ),
            title: Text(
              '12.00 A.M.',style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'hello world',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print('dangerous');
            },
          ),
        ],
      ),
    );
  }
}
